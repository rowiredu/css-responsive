<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600&family=Nunito:100,200,400,500,600,700,800" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/app.css">
        <!-- Styles -->
        <style>
            *{
                -webkit-font-smoothing: antiliased;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', 'Raleway', sans-serif;
                font-weight: 700;
                margin: 0;
                box-sizing: border-box;
                padding: 0;
            }
        </style>
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
