@extends('layouts.app')

<div class="w">
    <div class="c z">
        <h2 class="h">Make it breath</h2>
        <p class="p">Your design needs to breath</p>
        <span class="cc a"></span>
        <span class="pf">20% off</span>
        <span class="u"></span>
    </div>
    <div class="c">
        <h2 class="h">Make it catchy</h2>
        <p class="p">Your design needs to breath</p>
        <span class="sq a"></span>
        <span class="pf">20% off</span>
        <span class="u"></span>
    </div>
    <div class="c">
        <h2 class="h">Make it clear</h2>
        <p class="p">Your design needs to breath</p>
        <span class="tg a"></span>
        <span class="pf">20% off</span>
        <span class="u"></span>
    </div>
</div>
<span class="ml">Games</span>
<style>
    @media screen and (min-width: 1200px) {
        .pf {
            width: 120px;
            height: 20px;
            background: #00b6ff;
            position: absolute;
            top: 10;
            left: -30;
            transform: rotate(-45deg);
            text-align: left;
            padding-left: 30px;
            font-weight: 800;
            font-size: 12px;
            color: white;
            transition: all 0.35s ease-in;
        }

        .a {
            position: absolute;
        }

        .ml {
            /*height: 5px;*/
            /*background: #6f4949;*/
            /*background: #a324b3;*/
            position: absolute;
            display: flex;
            top: 50%;
            width: 100%;
            z-index: 0;
            text-transform: uppercase;
            font-size: 200px;
            font-weight: 700;
            padding-left: 32px;
            transition: color 0.1s ease-in-out;
        }

        .u {
            height: 5px;
            background: #fff;
            /*background: #3e3e3e;*/
            width: 40px;
            margin-left: 55px;
        }

        .w {
            display: flex;
            height: 100vh;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            background: linear-gradient(30deg, #363c40 40%, #2e4044 50%);
            /*background: #04141b;*/
            transition: all 0.35s ease-in-out;
        }

        .c {
            display: flex;
            align-items: flex-start;
            justify-content: center;
            height: 250px;
            width: 250px;
            background: #1b1d1f;
            /*background: #fff;*/
            border-radius: 30px;
            flex-direction: column;
            margin: 0px 70px;
            transition: all 0.5s ease-in-out;
            zoom: 1;
            z-index: 1000;
            /*border: 5px solid #a324b3;*/
            position: relative;
            overflow: hidden;
        }

        .c:hover {
            transform: scale(1.3);
        }

        .z {
            transform: scale(1.3);
        }

        .c:hover ~ .c.z {
            transform: scale(1);
        }
   
        .h {
            text-transform: uppercase;        
            font-weight: 800;
            color: #fff;
            /*color: #3e3e3e;*/
            margin-left: 55px;
            width: 100px;
            font-size: 22px;
        }

        .p {
            width: 80px;
            margin-left: 55px;
            line-height: 1.2em;
            font-size: 12px;
        }
    }

    @media screen and (max-width: 900px) and (min-width: 601px) {
        .ml {
            display: none;
        }

        .pf {
            width: 120px;
            height: 20px;
            background: #00b6ff;
            position: absolute;
            top: 10;
            left: -30;
            transform: rotate(-45deg);
            text-align: left;
            padding-left: 30px;
            font-weight: 800;
            font-size: 12px;
            color: white;
            transition: all 0.35s ease-in;
        }

        .u {
            height: 5px;
            background: #fff;
            width: 40px;
            margin-left: 55px;
        }

        .w {
            display: grid;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            transition: all 0.1s ease-in-out;
            grid-template-columns: auto auto;
            grid-gap: 70px;
            padding: 70px 0px 70px 0px;
        }

        .c {
            display: flex;
            align-items: flex-start;
            justify-content: center;
            height: 250px;
            width: 250px;
            background: #3e3e3e;
            border-radius: 30px;
            flex-direction: column;
            transition: all 0.5s ease-in-out;
            position: relative;
            overflow: hidden;
        }
        
        .h {
            text-transform: uppercase;        
            font-weight: 800;
            color: #fff;
            margin-left: 55px;
            width: 100px;
            font-size: 24px;
        }

        .p {
            width: 100px;
            margin-left: 55px;
            line-height: 1.2em;
        }
    }

    @media screen and (min-width: 901px) and (max-width: 1200px) {
        .ml {
            display: none;
        }

        .pf {
            width: 60px;
            border-radius: 30px;
            background: #00b6ff;
            position: absolute;
            text-align: left;
            display: grid;
            bottom: 20px;
            right: 20px;
            font-weight: 800;
            font-size: 10px;
            color: white;
            transition: all 0.35s ease-in;
            justify-content: center;
            align-items: center;;
            padding: 6px 0px;
        }

        .u {
            height: 5px;
            background: #fff;
            width: 35px;
            margin-left: 40px;
        }

        .w {
            display: flex;
            height: 100vh;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            background: #00b6ff;
            transition: all 0.35s ease-in;
        }

        .c {
            display: flex;
            position: relative;
            align-items: flex-start;
            justify-content: center;
            height: 200px;
            width: 200px;
            background: #3e3e3e;
            border-radius: 30px;
            flex-direction: column;
            margin: 0px 50px;
            transition: all 0.5s ease-in;
        }

        .h {
            text-transform: uppercase;        
            font-weight: 800;
            color: #fff;
            margin-left: 40px;
            width: 100px;
            font-size: 20px;
        }

        .p {
            width: 80px;
            margin-left: 40px;
            line-height: 1.2em;
            font-size: 12px;
        }
    }

    @media screen and (max-width: 600px) {
        .ml {
            display: none;
        }

        .pf {
            width: 120px;
            border-radius: 30px;
            background: #00b6ff;
            position: absolute;
            text-align: left;
            display: grid;
            bottom: 20px;
            right: 20px;
            font-weight: 800;
            font-size: 22px;
            color: white;
            transition: all 0.35s ease-in;
            justify-content: center;
            align-items: center;
            padding: 0px 20px;
        }
        
        .u {
            height: 5px;
            background: #fff;
            width: 40px;
            margin-left: 55px;
        }

        .w {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            background: #00b6ff;
            flex-direction: column;
            background: none;
            transition: all 0.35s ease-in-out;
        }

        .c {
            display: flex;
            align-items: flex-start;
            justify-content: center;
            height: 250px;
            width: 100%;
            background: #3e3e3e;
            border-radius: 0px;
            flex-direction: column;
            margin: 0px 0px;
            transition: all 0.5s ease-in-out;
            border-bottom: 2px solid #636b6f;
            position: relative;
        }
        
        .h {
            text-transform: uppercase;        
            font-weight: 800;
            color: #fff;
            margin-left: 55px;
            width: 100px;
            font-size: 24px;
        }

        .p {
            width: 100px;
            margin-left: 55px;
            line-height: 1.2em;
        }
    }

</style>